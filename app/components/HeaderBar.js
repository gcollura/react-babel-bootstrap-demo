'use strict';

import React from 'react';
import { Navbar, NavBrand, CollapsibleNav, Nav,
  NavItem, NavDropdown, MenuItem } from 'react-bootstrap';

class HeaderBar extends React.Component {
  render() {
    return (
      <Navbar fixedTop={true} fluid={true} toggleNavKey={0}>
        <NavBrand><a href="#">React-Bootstrap</a></NavBrand>
        <CollapsibleNav eventKey={0}>
          <Nav navbar right>
            <NavItem eventKey={1} href="#">Link</NavItem>
            <NavItem eventKey={2} href="#">Link</NavItem>
            <NavDropdown eventKey={3} title="Dropdown" id="basic-nav-dropdown">
              <MenuItem href='/' eventKey="1">Action</MenuItem>
              <MenuItem eventKey="2">Another action</MenuItem>
              <MenuItem eventKey="3">Something else here</MenuItem>
              <MenuItem divider />
              <MenuItem eventKey="4">Separated link</MenuItem>
            </NavDropdown>
          </Nav>
        </CollapsibleNav>
      </Navbar>
    );
  }
}

export default HeaderBar;
