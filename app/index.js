'use strict';

import React from 'react';
import ReactDOM from 'react-dom';
import HelloWorld from './components/HelloWorld';
import HeaderBar from './components/HeaderBar';

const mountNode = document.getElementById('content');

ReactDOM.render(
  <div>
    <HeaderBar/>
    <div className='container-fluid'>
      <HelloWorld/>
    </div>
  </div>,
  mountNode
);
