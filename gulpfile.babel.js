'use strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import uglify from 'gulp-uglify';
import browserify from 'browserify';
import source from 'vinyl-source-stream';
import buffer from 'vinyl-buffer';
import babelify from 'babelify';

const paths = {
  bundle: 'app.js',
  srcJsx: 'app/index.js',
  srcCss: 'app/**/*.css',
  srcImg: 'app/images/**',
  dist: 'dist',
  distJs: 'dist/js',
  distImg: 'dist/images',
  distCss: 'dist/css'
};

gulp.task('browserify', () => {
  return browserify(paths.srcJsx)
    .transform(babelify)
    .bundle()
    .pipe(source(paths.bundle))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(paths.distJs));
});

gulp.task('styles', () => {
  return gulp.src('scss/**/*.scss')
    .pipe(sass({
      errLogToConsole: true,
      includePaths: [
        'node_modules/bootstrap-sass/assets/stylesheets'
        // path.join(config.bowerDir, '/font-awesome/scss')
      ]
    }))
    .on('error', function(err) {
      console.log(err.message);
      this.emit('end');
    })
    // .pipe(concat('style.css'))
    .pipe(gulp.dest(paths.distCss));
});

gulp.task('default', [ 'browserify' ]);
